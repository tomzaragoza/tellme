import twilio.twiml
from flask import Flask, request
from pymongo import MongoClient
import requests
import xml.etree.ElementTree as ElementTree
#from lxml import objectify
app = Flask(__name__)

mongo = MongoClient("mongodb://IbmCloud_dcimg1f7_m6cl0hft_o33pu9ra:fNCzxdIzxbhn-6rIY4J1YFVskL5x-Ac0@ds035760.mongolab.com:35760/IbmCloud_dcimg1f7_m6cl0hft")
db = mongo.IbmCloud_dcimg1f7_m6cl0hft

class XmlListConfig(list):
	def __init__(self, aList):
		for element in aList:
			if element:
				# treat like dict
				if len(element) == 1 or element[0].tag != element[1].tag:
					self.append(XmlDictConfig(element))
				# treat like list
				elif element[0].tag == element[1].tag:
					self.append(XmlListConfig(element))
			elif element.text:
				text = element.text.strip()
				if text:
					self.append(text)


class XmlDictConfig(dict):
	'''
	Example usage:

	>>> tree = ElementTree.parse('your_file.xml')
	>>> root = tree.getroot()
	>>> xmldict = XmlDictConfig(root)

	Or, if you want to use an XML string:

	>>> root = ElementTree.XML(xml_string)
	>>> xmldict = XmlDictConfig(root)

	And then use xmldict for what it is... a dict.
	'''
	def __init__(self, parent_element):
		if parent_element.items():
			self.update(dict(parent_element.items()))
		for element in parent_element:
			if element:
				# treat like dict - we assume that if the first two tags
				# in a series are different, then they are all different.
				if len(element) == 1 or element[0].tag != element[1].tag:
					aDict = XmlDictConfig(element)
				# treat like list - we assume that if the first two tags
				# in a series are the same, then the rest are the same.
				else:
					# here, we put the list in dictionary; the key is the
					# tag name the list elements all share in common, and
					# the value is the list itself 
					aDict = {element[0].tag: XmlListConfig(element)}
				# if the tag has attributes, add those to the dict
				if element.items():
					aDict.update(dict(element.items()))
				self.update({element.tag: aDict})
			# this assumes that if you've got an attribute in a tag,
			# you won't be having any text. This may or may not be a 
			# good idea -- time will tell. It works for the way we are
			# currently doing XML configuration files...
			elif element.items():
				self.update({element.tag: dict(element.items())})
			# finally, if there are no child tags and no attributes, extract
			# the text
			else:
				self.update({element.tag: element.text})


@app.route("/", methods=['GET', 'POST'])
def call():
	from_number = request.values.get('From', None)
	#Idea: maybe check in the groceries store if the person is a member by the cellphone. Giving more personalization like: Hi, Mr./Ms. X, great to see you again  -> With this we would create more value for the product	

	bodyMsg = request.values.get('Body', None)
	# bodyMsg = "what do you recommend"


	url_list = [
		"https://mnutsch-sentence-recognition.p.mashape.com/sentencerecognition.php?input={0}&sentence1=how+much+is&sentence2=what+is+the+price+of&sentence3=what+is+the+cost+of".format(bodyMsg),
		"https://mnutsch-sentence-recognition.p.mashape.com/sentencerecognition.php?input={0}&sentence1=where+is&sentence2=where+can+i+find&sentence3=show+me+where".format(bodyMsg),
		"https://mnutsch-sentence-recognition.p.mashape.com/sentencerecognition.php?input={0}&sentence1=what+is+similar+to&sentence2=what+is+the+same+as&sentence3=what+is+like".format(bodyMsg),
		"https://mnutsch-sentence-recognition.p.mashape.com/sentencerecognition.php?input={0}&sentence1=what+do+you+recommend&sentence2=what+should+i+get&sentence3=what+to+buy".format(bodyMsg)
	]

	i = 0
	while i < 4:
		request_url = url_list[i]

		# These code snippets use an open-source library. http://unirest.io/python
		response = requests.get(request_url,

		  headers={
			"X-Mashape-Key": "EXmAQvzqdSmshunilH1GCTgY5XuTp1gFuY7jsnaZi8zRAY9h3w",
			"Accept": "text/plain"
		  }
		)
		

		root = ElementTree.XML(response.text)
		xmldict = XmlDictConfig(root)

		dictChks = xmldict["prompts"]
		arrChks = dictChks["prompt"]
		from pprint import pprint
		pprint(dictChks)
		pprint(arrChks)

		chck1 = float(arrChks[0]["prompt_score"])
		chck2 = float(arrChks[1]["prompt_score"])
		chck3 = float(arrChks[2]["prompt_score"])

		if chck1 >= 75.0 or chck2 >= 75.0 or chck3 >= 75.0:
			products = db.products
			response_message = ""

			if i == 0: # price
				# do text search
				product_query = bodyMsg.split(" ")[-1]
				# print product_query
				product = products.find_one({"$text":{"$search": product_query}})
				if not product:
					pass
				else:
					response_message += "Price of " + product_query + " is " + product["product_price"]

			elif i == 1: #isle (maincategory)
				product_query = bodyMsg.split(" ")[-1]
				product = products.find_one({"$text":{"$search": product_query}})

				if not product:
					pass
				else:
					response_message += product_query + " can be found in the " + product["category"] + " aisle"

			elif i == 2: #similar product
				product_query = bodyMsg.split(" ")[-1]
				product = products.find_one({"$text":{"$search": product_query}})

				if not product:
					pass
				else:
					response_message += "Similar products are: -" + "\n -".join(product['similar_products'])

			elif i == 3: #customer product recommendations
				customers = db.customers
				if "+1" in str(from_number):
					customer = customers.find_one({"number":str(from_number)})
				else:
					customer = customers.find_one({"number":"+1"+str(from_number)})

				if not customer:
					pass
				else:
					response_message += "Recommended products are: -" + "\n -".join(customer["product_recommendations"])

			resp = twilio.twiml.Response()
			resp.message(response_message)
			return str(resp)

		i += 1


	resp = twilio.twiml.Response()
	resp.message("Sorry, we didn't understand that. Please try again.")
	return str(resp)


if __name__ == "__main__":
	app.run(debug=True)



