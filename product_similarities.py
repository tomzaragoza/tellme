from pymongo import MongoClient
from random import randint

mongo = MongoClient()

db = mongo.uofthacks2015
collection = db.products


def product_similarities(collection):
	""" just have 5 from the same subcategory """

	for product in collection.find():

		product_name = product['product_name']
		subcategory = product['subcategory']
		similar_products = []

		print product_name
		l = list(collection.find({'subcategory': subcategory}))

		i = 0
		while i <= 5:	
			similar_products.append(l[randint(0, len(l)-1)]['product_name'])
			i += 1

		collection.update({'product_name': product_name}, {'$set': {'similar_products': similar_products}})


if __name__ == '__main__':
	product_similarities(collection)