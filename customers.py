from pymongo import MongoClient

mongo = MongoClient()

db = mongo.uofthacks2015
collection = db.customers


collection.insert(
	{
		"number": "+16478347784",
		"queries": [],
		"product_recommendations": [],
		"past_purchases": [
					"Bananas, Bunch", 
					"Royal Gala Apples", 
					"Farmer's Market Two-Bite Brownies (300g)",
					"Rudolph's Light Rye Bread (907g)",
					"Neilson 2% Milk (4L)",
					"No Name Grade A Brown Eggs, Large (12ea)",
					"Heinz British Style Baked Beans (398mL)",
					"President's Choice Blue Menu Instant Oatmeal, Original (360g)"
					"Tostitos Restaurant Style Tortilla Chips (300g)"
				]
	})

collection.insert(
	{
		"number": "+15195740728",
		"queries": [],
		"product_recommendations": [],
		"past_purchases": [
							"Hunt's Snackpack Juicy Gels, Cherry Lemon-Lime (4x99g)", 
							"Glutino Gluten-Free Pretzels, Snack Pack (75g)", 
							"Stand Up Pouch (165g)", 
							"Christie Triscuit Triscuit, Less Salt (225g)", 
							"Del Monte Fruit Salad Cups (4x142mL)", 
							"Quaker Chewy Granola Bars, S'Mores (156g)"
						]
	})
	
