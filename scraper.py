from bs4 import BeautifulSoup
import urllib2
import requests
from pymongo import MongoClient

mongo_client = MongoClient()

urls = [
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Fruit/c/LSL001001001000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Vegetable/c/LSL001001002000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Packaged-Salads/plp/LSL001001003000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Fresh-Herbs/plp/LSL001001004000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Organic-Fruit/plp/LSL001001005000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Organic-Vegetables/plp/LSL001001006000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Fresh-Cut-Fruit-%26-Veg/plp/LSL001001007000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Specialty-Vegetables/plp/LSL001001008000",
		"https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Dried-Fruits-%26-Nuts/plp/LSL001001010000",

		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Deli-Meats/c/LSL001002001000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Deli-Cheese/c/LSL001002002000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Antipastos%2C-Dips-%26-Spreads/c/LSL001002003000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Vegan-%26-Vegetarian/c/LSL001002004000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Ready-Meals-%26-Sides/c/LSL001002005000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Fresh-Pizza%2C-Pasta-%26-Sauces/c/LSL001002006000",
		"https://shop.loblaws.ca/Food/Deli-%26-Ready-Meals/Platters/plp/LSL001002007000",

		"https://shop.loblaws.ca/Food/Bakery/Freshly-Baked-Breads/c/LSL001003001000",
		"https://shop.loblaws.ca/Food/Bakery/Packaged-Breads/c/LSL001003002000",
		"https://shop.loblaws.ca/Food/Bakery/Baguettes%2C-Buns-%26-Rolls/c/LSL001003004000",
		"https://shop.loblaws.ca/Food/Bakery/Wraps%2C-Pita-%26-Flat-Breads/c/LSL001003005000",
		"https://shop.loblaws.ca/Food/Bakery/Breakfast%2C-Muffins%2C-%26-Scones/c/LSL001003006000",
		"https://shop.loblaws.ca/Food/Bakery/Desserts-%26-Pastries/c/LSL001003007000",
		"https://shop.loblaws.ca/Food/Bakery/Bread-Crumbs-%26-Stuffing/plp/LSL001003003000",
		"https://shop.loblaws.ca/Food/Bakery/Gluten-Free/c/LSL001003008000",

		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Beef-%26-Veal/c/LSL001004001000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Chicken-%26-Turkey/c/LSL001004002000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Pork/c/LSL001004003000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Lamb-%26-Goat/c/LSL001004004000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Fish-%26-Seafood/c/LSL001004006000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Game-Meats-%26-Fowl/c/LSL001004010000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Prepped-%26-Marinated/c/LSL001004009000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Free-From-Meats/plp/LSL001004005000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Fresh-Meat-Counter/plp/LSL001004007000",
		"https://shop.loblaws.ca/Food/Meat-%26-Seafood/Bacon/plp/N001004012001"

		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Milk-%26-Cream/c/LSL001005005000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Butter-%26-Spreads/c/LSL001005001000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Egg-%26-Egg-Substitutes/c/LSL001005003000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Packaged-Cheeses/c/LSL001005007000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Yogurt/c/LSL001005009000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Sour-Creams-%26-Dips/c/LSL001005008000",
		"https://shop.loblaws.ca/Food/Dairy-and-Eggs/Chilled-Desserts-%26-Doughs/c/LSL001005002000",

		"https://shop.loblaws.ca/Food/Drinks/Coffee/c/LSL001006001000",
		"https://shop.loblaws.ca/Food/Drinks/Drink-Mixes/c/LSL001006002000",
		"https://shop.loblaws.ca/Food/Drinks/Iced-Teas-%26-Coffees/c/LSL001006003000",
		"https://shop.loblaws.ca/Food/Drinks/Juice/c/LSL001006004000",
		"https://shop.loblaws.ca/Food/Drinks/Soft-Drinks/c/LSL001006005000",
		"https://shop.loblaws.ca/Food/Drinks/Soy%2C-Rice-%26-Nut-Drinks/c/LSL001006006000",
		"https://shop.loblaws.ca/Food/Drinks/Sports-and-Energy-Drinks/c/LSL001006007000",
		"https://shop.loblaws.ca/Food/Drinks/Tea-%26-Hot-Drinks/c/LSL001006008000",

		"https://shop.loblaws.ca/Food/Frozen/Appetizers-%26-Snacks/c/LSL001007001000",
		"https://shop.loblaws.ca/Food/Frozen/Bakery-%26-Fruit/c/LSL001007002000",
		"https://shop.loblaws.ca/Food/Frozen/Beverages-%26-Ice/c/LSL001007003000",
		"https://shop.loblaws.ca/Food/Frozen/Breakfast-Foods/c/LSL001007004000",
		"https://shop.loblaws.ca/Food/Frozen/Fish-%26-Seafood/c/LSL001007005000",
		"https://shop.loblaws.ca/Food/Frozen/Ice-Cream-%26-Treats/c/LSL001007006000",
		"https://shop.loblaws.ca/Food/Frozen/Meals/c/LSL001007007000",
		"https://shop.loblaws.ca/Food/Frozen/Meat-%26-Poultry/c/LSL001007008000",
		"https://shop.loblaws.ca/Food/Frozen/Vegetables/c/LSL001007010000",

		"https://shop.loblaws.ca/Food/Pantry/Baby-Food-%26-Formula/c/LSL001008001000",
		"https://shop.loblaws.ca/Food/Pantry/Baking-Ingredients/c/LSL001008002000",
		"https://shop.loblaws.ca/Food/Pantry/Canned-%26-Jarred/c/LSL001008003000",
		"https://shop.loblaws.ca/Food/Pantry/Cereal%2C-Spreads-%26-Syrups/c/LSL001008004000",
		"https://shop.loblaws.ca/Food/Pantry/Condiments-%26-Toppings/c/LSL001008005000",
		"https://shop.loblaws.ca/Food/Pantry/Herbs%2C-Spices-%26-Sauces/c/LSL001008006000",
		"https://shop.loblaws.ca/Food/Pantry/Oil%2C-Vinegars-%26-Cooking-Wine/c/LSL001008007000",
		"https://shop.loblaws.ca/Food/Pantry/Packaged-Meals-%26-Kits/c/LSL001008008000",
		"https://shop.loblaws.ca/Food/Pantry/Pasta%2C-Rice-%26-Beans/c/LSL001008009000",
		"https://shop.loblaws.ca/Food/Pantry/Snacks-%26-Candy/c/LSL001008010000"
	]

for url in urls:
	# url = "https://shop.loblaws.ca/Food/Fruits-%26-Vegetables/Fruit/c/LSL001001001000"
	print url
	f = requests.get(url)
	soup = BeautifulSoup(f.text,'html5lib')

	# subcategory = soup.find("div", {"id":"js-hero-title"}).find("h1").text
	url_decoded = urllib2.unquote(url)

	subcategory = url.split("/")[-3].replace("-", " ")
	category = url_decoded.split("/")[-4].replace("-", " ")


	items = soup.findAll("div", {"class": "module-grocery-product"})


	for item in items:

		db = mongo_client.uofthacks2015
		collection = db.products

		product_info_div = item.find("div", {"class": "product-info"})

		# Get the product price and name
		product_name = product_info_div.find("h2", {"class":"product-name"}).find("a").text.strip()
		product_price = product_info_div.find("div", {"class":"price"}).find("span").text.strip()

		# Get the product image
		product_image = item.find("img").get("src")

		product = {
					"category": category,
					"subcategory": subcategory,
					"product_name": product_name,
					"product_price": product_price,
					"product_image": product_image,
					"similar_products": []
				}

		if not collection.find_one({"product_name": product_name}):
			collection.insert(product)



