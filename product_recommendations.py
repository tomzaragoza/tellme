from pymongo import MongoClient
from random import randint

mongo = MongoClient()

db = mongo.uofthacks2015
collection = db.customers


def recommender(past_purchases):
	pass


def product_recommendations(collection):
	for customer in collection.find():
		number = customer["number"]

		past_purchases = customer['past_purchases']
		product_recommendations = []

		for p in past_purchases:
			product = db.products.find_one({"product_name":p})
			if product:
				product_recommendations.append(product['similar_products'][randint(0, len(product['similar_products'])-1)])


		collection.update({"number": number}, {"$set": {"product_recommendations":product_recommendations}})


if __name__ == '__main__':
	product_recommendations(collection)

